Masivian Technical Test
======================

:star: Enjoyed doing this test!

Technical test resolved implementing a Binary Tree with a REST API. 

## Installation

In order to install the app run the command:

```
mvn install
```

Navigate to target folder and execute in a command window
```
java -jar technical-test-0.0.1.jar
```

Application will start on port 9000.
## Usage and Examples

In order to generate a tree based on a list of numbers use:
```
http://locahost:9000/tree
```
The input must be like:
```
[87,29,28,44,39,76,74,85,67,83]
```
The REST service will generate a Binary Tree JSON

```json
{
    "root": {
        "key": 67,
        "left": {
            "key": 29,
            "left": {
                "key": 28,
                "left": null,
                "right": null
            },
            "right": {
                "key": 39,
                "left": null,
                "right": {
                    "key": 44,
                    "left": null,
                    "right": null
                }
            }
        },
        "right": {
            "key": 83,
            "left": {
                "key": 74,
                "left": null,
                "right": {
                    "key": 76,
                    "left": null,
                    "right": null
                }
            },
            "right": {
                "key": 85,
                "left": null,
                "right": {
                    "key": 87,
                    "left": null,
                    "right": null
                }
            }
        }
    }
}
```

In order to get the Lowest Common Ancestor use:
```
http://localhost:9000/lowestCommonAncestor
```
The JSON input must be like
```json
{
    "tree":{
    		"root": {
		        "key": 67,
		        "left": {
		            "key": 29,
		            "left": {
		                "key": 28,
		                "left": null,
		                "right": null
		            },
		            "right": {
		                "key": 39,
		                "left": null,
		                "right": {
		                    "key": 44,
		                    "left": null,
		                    "right": null
		                }
		            }
		        },
		        "right": {
		            "key": 83,
		            "left": {
		                "key": 74,
		                "left": null,
		                "right": {
		                    "key": 76,
		                    "left": null,
		                    "right": null
		                }
		            },
		            "right": {
		                "key": 85,
		                "left": null,
		                "right": {
		                    "key": 87,
		                    "left": null,
		                    "right": null
		                }
		            }
		        }
		    },
	"number1": 83,
	"number2": 87
}
```
It will compute a Lowest Common Ancestor Algorithm