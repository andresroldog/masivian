/*
 * This class was build by Andrés Motavita
 * for Masivian technical test. 19/02/2019
 */
package com.masivian.developers.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.masivian.developers.model.LowestCommonAncestorInput;
import com.masivian.developers.model.Tree;
import com.masivian.developers.service.TreeService;

/**
 * Rest Controller Class
 * 
 * @author Andrés Felipe Motavita
 *
 */
@RestController
@RequestMapping(value = "/", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
public class ForestController {
	/**
	 * Tree service
	 */
	@Autowired
	private TreeService treeService;

	/**
	 * Given a list of numbers, generates a binary tree
	 * 
	 * @param values
	 * @return Binary tree
	 */
	@PostMapping(value = "/tree")
	public Tree getTree(@RequestBody List<Integer> values) {
		return treeService.getTree(values);
	}

	/**
	 * Given a binary tree and two numbers, returns the Lowest Common Ancestor key
	 * 
	 * @param input
	 * @return if exists the Lowest Common Ancestor key, otherwise null
	 */
	@PostMapping(value = "/lowestCommonAncestor")
	public int getLCA(@RequestBody LowestCommonAncestorInput input) {
		return treeService.getLowestCommonAncestor(input.getTree(), input.getNumber1(), input.getNumber2());
	}
}
