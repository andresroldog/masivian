/*
 * This class was build by Andrés Motavita
 * for Masivian technical test. 19/02/2019
 */
package com.masivian.developers.model;

/**
 * 
 * Model Class for Node structure
 * 
 * @author Andrés Felipe Motavita
 * 
 */
public class Node {
	/**
	 * Node key
	 */
	private int key;
	/**
	 * Left child
	 */
	private Node left;
	/**
	 * Right child
	 */
	private Node right;

	/**
	 * Default constructor
	 */
	public Node() {

	}

	/**
	 * Constructor with key parameter
	 * 
	 * @param key
	 */
	public Node(int key) {
		this.key = key;
		this.left = this.right = null;
	}

	/**
	 * Getter Key
	 * 
	 * @return key
	 */
	public int getKey() {
		return key;
	}

	/**
	 * Setter key
	 * 
	 * @param key
	 */
	public void setKey(int key) {
		this.key = key;
	}

	/**
	 * Getter left
	 * 
	 * @return left
	 */
	public Node getLeft() {
		return left;
	}

	/**
	 * Setter left
	 * 
	 * @param left
	 */
	public void setLeft(Node left) {
		this.left = left;
	}

	/**
	 * Getter right
	 * 
	 * @return right
	 */
	public Node getRight() {
		return right;
	}

	/**
	 * Setter right
	 * 
	 * @param right
	 */
	public void setRight(Node right) {
		this.right = right;
	}

}