/*
 * This class was build by Andrés Motavita
 * for Masivian technical test. 19/02/2019
 */
package com.masivian.developers.model;

import java.util.List;

/**
 * 
 * Model Class for Tree Structure
 * 
 * @author Andrés Motavita
 *
 */
public class Tree {
	/**
	 * Root node
	 */
	private Node root;

	/**
	 * Default constructor
	 */
	public Tree() {
		this.root = null;
	}

	/**
	 * Constructor with a list of integers as parameter
	 * 
	 * @param values
	 */
	public Tree(List<Integer> values) {
		this.root = buildFromList(values, 0, values.size() - 1);
	}

	/**
	 * Getter root
	 * 
	 * @return root
	 */
	public Node getRoot() {
		return root;
	}

	/**
	 * Setter root
	 * 
	 * @param root
	 */
	public void setRoot(Node root) {
		this.root = root;
	}

	/**
	 * Given a node and a key value, adds the node depending on the value of the key
	 * in the tree
	 * 
	 * @param node
	 * @param key
	 * @return node added
	 */
	private Node add(Node node, int key) {
		if (root == null) {
			root = new Node(key);
			return root;
		}
		if (node == null) {
			return new Node(key);
		}
		if (key < node.getKey()) {
			node.setLeft(add(node.getLeft(), key));
		} else if (key > node.getKey()) {
			node.setRight(add(node.getRight(), key));
		}
		return node;
	}

	/**
	 * Adds a key to the tree
	 * 
	 * @param key
	 * @return node added
	 */
	public Node add(int key) {
		return add(root, key);
	}

	/**
	 * Given two numbers, determines the Lowest Common Ancestor
	 * 
	 * @param node
	 * @param number1
	 * @param number2
	 * @return Lowest Commont Ancestor node, otherwise null
	 */
	public Node getLowestCommonAncestor(Node node, int number1, int number2) {
		if (node == null)
			return null;
		if (node.getKey() > number1 && node.getKey() > number2)
			return getLowestCommonAncestor(node.getLeft(), number1, number2);
		if (node.getKey() < number1 && node.getKey() < number2)
			return getLowestCommonAncestor(node.getRight(), number1, number2);
		return node;
	}

	/**
	 * Creates a tree structure from a list of numbers
	 * 
	 * @param values
	 * @param start
	 * @param end
	 * @return tree structure
	 */
	public Node buildFromList(List<Integer> values, int start, int end) {
		if (start > end)
			return null;
		int mid = (start + end) / 2;
		Node node = new Node(values.get(mid));
		node.setLeft(buildFromList(values, start, mid - 1));
		node.setRight(buildFromList(values, mid + 1, end));
		return node;
	}
}
