package com.masivian.developers.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TreeServiceTests {

	private static TreeService treeService;

	@BeforeClass
	public static void setUp() {
		treeService = mock(TreeService.class);
	}

	@Test
	public void whenEmptyList_thenNull() {
		List<Integer> values = new LinkedList<Integer>();
		when(treeService.getTree(values)).equals(null);
	}

}
