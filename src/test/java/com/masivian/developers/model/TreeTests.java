package com.masivian.developers.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TreeTests {

	private Tree tree;

	@Before
	public void setUp() {
		tree = new Tree();
	}

	@Test
	public void whenCreatedATree_thenEmptyRoot() {
		assertNull("Empty Root", tree.getRoot());
	}

	@Test
	public void whenNodeIsAdded_thenRootIsNotNull() {
		int key = 10;
		tree.add(key);
		assertNotNull("Root not null", tree.getRoot());
		assertNull("Left node empty", tree.getRoot().getLeft());
		assertNull("Right node empty ", tree.getRoot().getRight());
		assertEquals("Root populated", tree.getRoot().getKey(), key);
	}

	@Test
	public void whenNodeIsAddedWithNoEmptyTree_thenLeftNodeOfRootIsNotNull() {
		tree.add(10);
		int key = 9;
		tree.add(key);
		assertNotNull("Left node not null", tree.getRoot().getLeft());
		assertNull("Right node Empty", tree.getRoot().getRight());
		assertEquals("Left node key", tree.getRoot().getLeft().getKey(), key);
	}

	@Test
	public void whenNodeIsAddedWithNoEmptyTree_thenRightNodeOfRootIsNotNull() {
		tree.add(10);
		int key = 11;
		tree.add(key);
		assertNull("Left node empty", tree.getRoot().getLeft());
		assertNotNull("Right node not null", tree.getRoot().getRight());
		assertEquals("Right node key", tree.getRoot().getRight().getKey(), key);
	}

	@Test
	public void whenNodeIsNull_thenLCAIsNull() {
		Node node = tree.getLowestCommonAncestor(null, 1, 2);
		assertNull("LCA null", node);
	}

	@Test
	public void whenNodeIsLCA_thenNode() {
		tree.add(5);
		tree.add(1);
		tree.add(8);
		assertEquals("LCA", tree.getLowestCommonAncestor(tree.getRoot(), 1, 8).getKey(), 5);
	}

	@Test
	public void whenEndIsGreaterThanStart_thenNullNode() {
		assertNull("end is greater than start", tree.buildFromList(null, 10, 5));
	}

	@Test
	public void buildTreeFromList() {
		List<Integer> values = new LinkedList<>();
		values.add(8);
		values.add(1);
		values.add(5);
		tree.setRoot(tree.buildFromList(values, 0, 2));
	}

}
