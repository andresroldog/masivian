/*
 * This class was build by Andrés Motavita
 * for Masivian technical test. 19/02/2019
 */
package com.masivian.developers.service;

import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Service;

import com.masivian.developers.model.Tree;

/**
 * Service class
 * 
 * @author Andrés Felipe Motavita
 *
 */
@Service
public class TreeService {
	/**
	 * Given a list of numbers, sort them and creates a binary tree based on the
	 * sorted list
	 * 
	 * @param values
	 * @return Binary Tree
	 */
	public Tree getTree(List<Integer> values) {
		if (values.isEmpty())
			return null;
		values.sort(Comparator.naturalOrder());
		return new Tree(values);
	}

	/**
	 * Given a binary tree and two numbers, determines the Lowest Common Ancestor
	 * key
	 * 
	 * @param tree
	 * @param number1
	 * @param number2
	 * @return if exists the Lowest Common Ancestor key, otherwise null
	 */
	public int getLowestCommonAncestor(Tree tree, int number1, int number2) {
		return tree.getLowestCommonAncestor(tree.getRoot(), number1, number2).getKey();
	}

}
