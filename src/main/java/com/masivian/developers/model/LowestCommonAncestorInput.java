/*
 * This class was build by Andrés Motavita
 * for Masivian technical test. 19/02/2019
 */
package com.masivian.developers.model;

/**
 * Class for service input
 * 
 * @author Andrés Felipe Motavita
 *
 */
public class LowestCommonAncestorInput {
	/**
	 * Tree
	 */
	private Tree tree;
	/**
	 * Number 1
	 */
	private int number1;
	/**
	 * 
	 */
	private int number2;

	/**
	 * Default constructor
	 */
	public LowestCommonAncestorInput() {
	}

	/**
	 * Getter tree
	 * 
	 * @return tree
	 */
	public Tree getTree() {
		return tree;
	}

	/**
	 * Setter tree
	 * 
	 * @param tree
	 */
	public void setTree(Tree tree) {
		this.tree = tree;
	}

	/**
	 * Getter number 1
	 * 
	 * @return number1
	 */
	public int getNumber1() {
		return number1;
	}

	/**
	 * Setter number 1
	 * 
	 * @param number1
	 */
	public void setNumber1(int number1) {
		this.number1 = number1;
	}

	/**
	 * Getter number 2
	 * 
	 * @return number 2
	 */
	public int getNumber2() {
		return number2;
	}

	/**
	 * Setter number 2
	 * 
	 * @param number2
	 */
	public void setNumber2(int number2) {
		this.number2 = number2;
	}

}
