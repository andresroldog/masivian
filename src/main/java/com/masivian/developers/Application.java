/*
 * This class was build by Andrés Motavita
 * for Masivian technical test. 19/02/2019
 */
package com.masivian.developers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application main class
 * 
 * @author Andrés Felipe Motavita
 *
 */
@SpringBootApplication
public class Application {
	/**
	 * Main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
